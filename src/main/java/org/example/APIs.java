package org.example;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import java.util.*;

public class APIs {
    public static void producer(){
        // message's details to send
        String topicName = "streams-plaintext";
        String key = "Key";
        String value = "Value";

        // set properties up
        Properties props = new Properties();
        // bootstrap: kafka broker needs to be added here
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "c6801.ambari.apache.org:6667");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // creating producer object. <Key, Value>. also we need to add properties here into producer object
        Producer<String, String> producer = new KafkaProducer<>(props);

        // sending messages
        for (int i = 0; i < 10; i++) {
            // creating producer record object to send message's details
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topicName, key + i, value + i);
            // and finally we are sending the message
            producer.send(producerRecord);
        }

        // closing producer
        producer.close();
        System.out.println("Producer has sent the message");
    }

    public static void consumer(){
        // topic name and consumer group name
        String groupName = "Consumer-Group-Name";
        String topicName = "streams-plaintext";

        // set properties up as above
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "c6801.ambari.apache.org:6667");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupName);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // it is time to deserialize the received bytes
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // creating kafka consumer object
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(topicName)); // here consumer subscribe the topic

        // creating infinite loop to listen kafka broker and receive messages continuously
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100); // if no data, return in 100 ms. poll is group coordinator
            for (ConsumerRecord<String, String> record : records) {
                System.out.println("Supplier value= " + record.value() + "\tSupplier topic name= " + record.topic());
            }
        }
    }
}
